from tensorflow import keras
from keras import Model
from keras.applications.vgg19 import VGG19
import numpy as np
import matplotlib.image as mpimg
from matplotlib import pyplot as plt

from keras.utils import load_img

def get_image_array(path):
    img = load_img(path, target_size=(224, 224, 3))
    img_array = np.array(img)
    plt.imshow(img_array)
    plt.show()
    return img_array

def plot_image_from_array(array):
    plt.imshow(array)
    plt.show()

model = VGG19(include_top=True, input_shape=(224, 224, 3), weights="imagenet")
#model.summary()

layers = model.layers

for layer in layers:
    print(layer.name)
    pass

visualization_model = Model(inputs = model.inputs, outputs = layers[1].output)

visualization_model.summary()

img_array = get_image_array("birb_for_activation_visualization.jpg")
img_array = img_array.reshape((1, 224, 224, 3))
print(img_array.shape)

prediction_array = visualization_model.predict(img_array)
print(prediction_array[0][:, :, 5].shape)

for i in range(64):
    plot_image_from_array(prediction_array[0][:, :, i])








